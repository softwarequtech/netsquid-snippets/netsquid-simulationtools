import unittest

from netsquid_simulationtools.linear_algebra import XYZEigenstateIndex, xyz_eigenstates
from netsquid_simulationtools.process_qkd import _do_we_expect_correlation, qber, _estimate_bb84_secret_key_rate, \
    _binary_entropy, estimate_bb84_secret_key_rate_from_data, _qber_from_state
from netsquid_simulationtools.repchain_data_functions import _expected_target_state
from tests.test_repchain_data_functions import ProcessingFunctionsTestBasis
import pandas
import numpy as np
import netsquid as ns
from netsquid.qubits.ketstates import b00, b01, b10, b11, BellIndex
import netsquid.qubits.qubitapi as qapi


class TestDoWeExpectCorrelation(ProcessingFunctionsTestBasis):

    def test_expected_correlations_unequal_bases(self):
        row = pandas.Series({"basis_A": "Z", "basis_B": "X"})
        with self.assertRaises(ValueError):
            _do_we_expect_correlation(row, basis_a=row["basis_A"], basis_b=row["basis_B"])

    def test_expected_correlations(self):
        """Tests Correctness of expected correlation for different states and basis choices."""
        # create list that contains each pauli correction once
        outcome_list = []
        for i in range(4):
            outcome_list.append([i, 0, 0, 0, 0])
        # basis dictionary
        basis = {1: "X",
                 2: "Y",
                 3: "Z"}
        # loop over all three basis
        for basis_index in [1, 2, 3]:
            data = self.SimData()

            # fill dataframe to get each bellstate once
            for l in range(len(outcome_list)):
                data.loc[l] = [basis[basis_index], basis[basis_index], 1, 1, 1] + outcome_list[l] + [1]

            # check if predicted correlations agree with expectation value of state
            for i in range(len(outcome_list)):
                row = data.iloc[i]
                do_we_expect_corr = _do_we_expect_correlation(row=row, basis_a=row["basis_A"], basis_b=row["basis_B"])
                bell_state = self.bell_state(_expected_target_state(row=row))

                bell_state_c = np.conjugate(bell_state)
                bell_state_T = np.transpose(bell_state_c)
                meas, = bell_state_T @ (self.operator(basis_index) ^ self.operator(basis_index)).arr @ bell_state
                if int(np.round(meas[0])) == -1:
                    self.assertFalse(do_we_expect_corr)
                else:
                    self.assertTrue(do_we_expect_corr)

            # while we have the dataframe, test agreement and qber
            qb, qber_error = qber(data, basis[basis_index])
            self.assertTrue(qb == 0.5)


class TestEstimateBB84SecretKeyRateFromData(ProcessingFunctionsTestBasis):
    def test_measurement_data(self):
        """Tests correctness of calculated secret key rate based on measurement data."""

        for qber_x in np.linspace(0, 1, 100):
            for qber_z in np.linspace(0, 1, 100):
                skr, skr_min, skr_max, skr_error = _estimate_bb84_secret_key_rate(qber_x, 0., qber_z, 0., 1, 0)
                self.assertEqual((max(0., 1 - _binary_entropy(qber_x) - _binary_entropy(qber_z))), skr)
                for i in [skr, skr_min, skr_max]:
                    self.assertTrue(max(i, 1.) == 1.)
                    self.assertTrue(min(i, 0.) == 0.)

        # fill outcomes in Dataframe with all possible combinations
        outcome_list = []
        for a in range(4):
            for b in range(4):
                for c in range(4):
                    for d in range(4):
                        for e in range(4):
                            outcome_list.append([a, b, c, d, e])
        data_x = self.SimData()
        for i in range(len(outcome_list)):
            data_x.loc[i] = ["X", "X", 1, 1, int(1)] + outcome_list[i] + [1]
        data_z = self.SimData()
        for i in range(len(outcome_list)):
            data_z.loc[i] = ["Z", "Z", 1, 1, int(1)] + outcome_list[i] + [1]
        data = pandas.concat([data_x, data_z])
        secret_key_rate, skr_min, skr_max, skr_error = estimate_bb84_secret_key_rate_from_data(data)
        self.assertEqual(secret_key_rate, 0.)

    def test_perfect_measurement_correlation(self):
        data = {"midpoint_outcome_0": [BellIndex.B00, BellIndex.B11, BellIndex.B11, BellIndex.B00],
                "generation_duration": [10, 10, 10, 10],
                "basis_A": ["Z", "Z", "X", "X"],
                "basis_B": ["Z", "Z", "X", "X"],
                "outcome_A": [0, 0, 1, 1],
                "outcome_B": [0, 1, 0, 1],
                }
        skr, skr_min, skr_max, skr_error = estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data))
        assert np.isclose(skr, 1 / 10)  # no error, 10 time steps per success
        assert np.isclose(skr_min, skr_max)
        assert np.isclose(skr_min, skr)
        assert skr_error == 0.

    def test_perfect_states(self):
        # perfect case
        data = {"state": [b00, b01, b10, b11],
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B01, BellIndex.B10, BellIndex.B11],
                "generation_duration": [10, 10, 10, 10]}
        skr, skr_min, skr_max, skr_error = estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data))
        assert np.isclose(skr, 1 / 10)  # no error, 10 time steps per success
        assert np.isclose(skr_min, skr_max)
        assert np.isclose(skr_min, skr)
        assert skr_error == 0.

    def test_perfect_state_different_durations(self):
        # different durations
        data = {"state": [b00, b01, b10, b11],
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B01, BellIndex.B10, BellIndex.B11],
                "generation_duration": [0, 20, 0, 20]}
        skr, skr_min, skr_max, skr_error = estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data))
        assert np.isclose(skr, 1 / 10)  # no error, 10 time steps per succes
        assert not np.isclose(skr_min, skr_max)
        assert not np.isclose(skr_min, skr)
        assert skr_error > 0

    def test_max_mixed_state(self):
        # maximally mixed
        data = {"state": [np.eye(4) / 4] * 4,
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B01, BellIndex.B10, BellIndex.B11],
                "generation_duration": [10, 10, 10, 10]}
        skr, skr_min, skr_max, skr_error = estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data))
        assert np.isclose(skr, 0.)
        assert np.isclose(skr_min, skr_max)
        assert np.isclose(skr_min, skr)
        assert skr_error == 0

    def test_state_and_measurement_data(self):
        data = {"state": [np.eye(4) / 4] * 4,
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B00, BellIndex.B00, BellIndex.B00],
                "generation_duration": [10, 10, 10, 10],
                "basis_A": ["Z", "Z", "X", "X"],
                "basis_B": ["Z", "Z", "X", "X"],
                "outcome_A": [0, 1, 1, 0],
                "outcome_B": [0, 1, 1, 0],
                }

        # when defaulting to state data, expect skr = 0 because of maximally mixed state
        skr_state, _, _, _ = estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data),
                                                                     use_state_data_as_default=True)
        assert np.isclose(skr_state, 0.)

        # when defaulting to measurement data, expect skr = 1 / 10 because of perfect correlations
        skr_measurement, _, _, _ = estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data),
                                                                           use_state_data_as_default=False)
        assert np.isclose(skr_measurement, 1 / 10)

    def test_invalid_state_but_good_measurement_data(self):
        data = {"state": [None, [0.], True, "some_state"],
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B00, BellIndex.B00, BellIndex.B00],
                "generation_duration": [10, 10, 10, 10],
                "basis_A": ["Z", "Z", "X", "X"],
                "basis_B": ["Z", "Z", "X", "X"],
                "outcome_A": [0, 1, 1, 0],
                "outcome_B": [0, 1, 1, 0],
                }
        skr, _, _, _ = estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data))
        assert np.isclose(skr, 1 / 10)

    def test_not_enough_measurement_data(self):
        data = {"midpoint_outcome_0": [BellIndex.B00, BellIndex.B00, BellIndex.B00, BellIndex.B00],
                "generation_duration": [10, 10],
                "basis_A": ["Z", "Z"],
                "basis_B": ["Z", "Z"],
                "outcome_A": [0, 1],
                "outcome_B": [0, 1]
                }
        with self.assertRaises(ValueError):
            estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data))

    def test_no_data(self):
        data = {"state": [None] * 4,
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B01, BellIndex.B10, BellIndex.B11],
                "generation_duration": [10, 10, 10, 10]}
        with self.assertRaises(ValueError):
            estimate_bb84_secret_key_rate_from_data(dataframe=pandas.DataFrame(data))


class TestQberFromState(unittest.TestCase):
    def test_00_state(self):
        # |00>
        state = [1., 0., 0., 0.]
        # if we expect correlation
        assert _qber_from_state(state=state, basis_a="Z", basis_b="Z", expect_correlation=True) == 0.
        # if we don't
        assert _qber_from_state(state=state, basis_a="Z", basis_b="Z", expect_correlation=False) == 1.
        # will be correlated in the X and Y bases half the time
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X", expect_correlation=True), .5)
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X", expect_correlation=False), .5)
        assert np.isclose(_qber_from_state(state=state, basis_a="Y", basis_b="Y", expect_correlation=True), .5)
        assert np.isclose(_qber_from_state(state=state, basis_a="Y", basis_b="Y", expect_correlation=False), .5)
        # for different measurement bases, will also be correlated half the time
        assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="X", expect_correlation=True), .5)
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="Z", expect_correlation=True), .5)
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="Y", expect_correlation=True), .5)

    def test_plusplus_state(self):
        # |++>
        state = np.kron(xyz_eigenstates[XYZEigenstateIndex.X0], xyz_eigenstates[XYZEigenstateIndex.X0])
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X", expect_correlation=True), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="Z", expect_correlation=True), .5)
        assert np.isclose(_qber_from_state(state=state, basis_a="Y", basis_b="Y", expect_correlation=True), .5)

    def test_yplusy_yplus_state(self):
        # |Y+Y+>
        state = np.kron(xyz_eigenstates[XYZEigenstateIndex.Y1], xyz_eigenstates[XYZEigenstateIndex.Y1])
        assert np.isclose(_qber_from_state(state=state, basis_a="Y", basis_b="Y", expect_correlation=True), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X", expect_correlation=True), .5)
        assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="Z", expect_correlation=True), .5)

    def test_b00_state(self):
        # |00> + |11>
        state = b00
        assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="Z", expect_correlation=True), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X", expect_correlation=True), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="Y", basis_b="Y", expect_correlation=False), 0.)

    def test_b01_state(self):
        # |01> + |10>
        state = b01
        assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="Z", expect_correlation=False), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X", expect_correlation=True), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="Y", basis_b="Y", expect_correlation=True), 0.)

    def test_b10_state(self):
        # |00> - |11>
        state = b10
        assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="Z", expect_correlation=True), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X", expect_correlation=False), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="Y", basis_b="Y", expect_correlation=True), 0.)

    def test_b11_state(self):
        # |01> - |10>
        state = b11
        assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="Z", expect_correlation=False), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X", expect_correlation=False), 0.)
        assert np.isclose(_qber_from_state(state=state, basis_a="Y", basis_b="Y", expect_correlation=False), 0.)

    def test_maxmixed_state(self):
        # max mixed state
        state = np.eye(4) / 4
        for basis_a in ["X", "Y", "Z"]:
            for basis_b in ["X", "Y", "Z"]:
                for expect_correlation in [True, False]:
                    assert np.isclose(_qber_from_state(state=state, basis_a=basis_a, basis_b=basis_b,
                                                       expect_correlation=expect_correlation),
                                      .5)

    @staticmethod
    def _prepare_state(state):
        ns.set_qstate_formalism(ns.QFormalism.DM)
        [q1, q2] = qapi.create_qubits(2)
        qapi.assign_qstate(qubits=[q1, q2], qrepr=state)
        return q1, q2

    def test_depolarized_state(self, depolar_prob=0.1):
        [q1, q2] = self._prepare_state(b00)
        qapi.depolarize(q1, prob=depolar_prob)
        state = qapi.reduced_dm([q1, q2])
        for basis in ["Z", "X", "Y"]:
            assert np.isclose(_qber_from_state(state=state, basis_a=basis, basis_b=basis,
                                               expect_correlation=basis != "Y"),
                              depolar_prob / 2)

    def test_dephased_state(self, dephasing_prob=0.1):
        for prepared_state, expect_correlation in zip([b00, b11], [True, False]):
            [q1, q2] = self._prepare_state(prepared_state)
            qapi.dephase(q1, prob=dephasing_prob)
            state = qapi.reduced_dm([q1, q2])
            assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="Z",
                                               expect_correlation=expect_correlation), 0.)
            assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X",
                                               expect_correlation=expect_correlation), dephasing_prob)

    def test_bit_flipped_state(self, bit_flip_prob=0.1):
        for prepared_state, expect_correlation in zip([b00, b11], [True, False]):
            [q1, q2] = self._prepare_state(prepared_state)
            qapi.apply_pauli_noise(q1, p_weights=(1 - bit_flip_prob, bit_flip_prob, 0., 0.))
            state = qapi.reduced_dm([q1, q2])
            assert np.isclose(_qber_from_state(state=state, basis_a="Z", basis_b="Z",
                                               expect_correlation=expect_correlation), bit_flip_prob)
            assert np.isclose(_qber_from_state(state=state, basis_a="X", basis_b="X",
                                               expect_correlation=expect_correlation), 0.)


class TestQber(unittest.TestCase):
    def test_perfect_state_data(self):
        data = {"state": [b00, b01, b10, b11],
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B01, BellIndex.B10, BellIndex.B11]}
        for basis in ["Z", "X", "Y"]:
            qb, qb_error = qber(dataframe=pandas.DataFrame(data), basis_a=basis, basis_b=None)
            assert np.isclose(qb, 0.)
            assert np.isclose(qb_error, 0.)

    def test_perfect_measurement_data(self):
        data = {
            "midpoint_outcome_0": [BellIndex.B00, BellIndex.B00, BellIndex.B00, BellIndex.B00,
                                   BellIndex.B01, BellIndex.B01, BellIndex.B11, BellIndex.B10],
            "basis_A": ["Z", "Z", "X", "X", "Y", "Y", "X", "Z"],
            "basis_B": ["Z", "Z", "X", "X", "Y", "Y", "Z", "Y"],
            "outcome_A": [0, 1, 1, 0, 0, 1, 1, 0],
            "outcome_B": [0, 1, 1, 0, 0, 1, 1, 0],
        }
        for basis in ["Z", "X", "Y"]:
            qb, qb_error = qber(dataframe=pandas.DataFrame(data), basis_a=basis, basis_b=None)
            assert np.isclose(qb, 0.)
            assert np.isclose(qb_error, 0.)

    def test_max_mixed_state_data(self):
        data = {"state": [np.eye(4) / 4] * 4,
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B01, BellIndex.B10, BellIndex.B11]}
        for basis in ["Z", "X", "Y"]:
            qb, qb_error = qber(dataframe=pandas.DataFrame(data), basis_a=basis, basis_b=None)
            assert np.isclose(qb, .5)
            assert np.isclose(qb_error, 0.)
