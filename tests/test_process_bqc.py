from netsquid_simulationtools.process_bqc import bqc_test_two_qubits_success_rate, \
    _two_qubit_bqc_test_success_probability, _average_fidelity_equator, _average_fidelity_poles
from netsquid.qubits.ketstates import b00, b01, b10, b11, BellIndex
import numpy as np
import unittest

from netsquid_simulationtools.repchain_dataframe_holder import RepchainDataFrameHolder


class TestBQCTestTwoQubitsSuccessRate(unittest.TestCase):
    def test_noiseless(self):
        data = {"state": [b00, b01, b10, b11],
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B01, BellIndex.B10, BellIndex.B11],
                "generation_duration": [10, 10, 10, 10]}
        dataframe = RepchainDataFrameHolder(data=data).dataframe
        success_rate, success_rate_error, success_prob, success_prob_error, rate, rate_error = \
            bqc_test_two_qubits_success_rate(dataframe=dataframe, coherence_time=0.)
        assert np.isclose(success_prob, 1.)
        assert np.isclose(success_rate, rate)
        assert np.isclose(rate, 1 / 20)  # distributing two pairs takes 20 seconds
        assert success_prob_error == 0.
        assert rate_error == 0.
        assert success_rate_error == 0.

    def test_half_success_probability(self):
        data = {"state": [b01, b01, b01, b01],
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B00, BellIndex.B00, BellIndex.B00],
                "generation_duration": [10, 10, 10, 10]}
        dataframe = RepchainDataFrameHolder(data=data).dataframe
        success_rate, success_rate_error, success_prob, success_prob_error, rate, rate_error = \
            bqc_test_two_qubits_success_rate(dataframe=dataframe, coherence_time=0.)
        assert np.isclose(success_prob, .5)
        assert np.isclose(rate, 1 / 20)
        assert np.isclose(success_rate, 1 / 20 / 2)
        assert success_prob_error == 0.
        assert rate_error == 0.
        assert success_rate_error == 0.

    def test_varied_generation_duration(self):
        data = {"state": [b00, b00, b00, b00],
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B00, BellIndex.B00, BellIndex.B00],
                "generation_duration": [1, 100, 13, 69]}
        dataframe = RepchainDataFrameHolder(data=data).dataframe
        success_rate, success_rate_error, success_prob, success_prob_error, rate, rate_error = \
            bqc_test_two_qubits_success_rate(dataframe=dataframe, coherence_time=10.)
        assert success_prob_error > 0.
        assert rate_error > 0.
        assert success_rate_error > 0.
        assert 0 < success_prob < 1
        assert np.isclose(rate, 2 / (1 + 100 + 13 + 69))
        assert np.isclose(success_rate, success_prob * rate)

    def test_varied_noise(self):
        data = {"state": [b00, b01, b10, b11],
                "midpoint_outcome_0": [BellIndex.B00, BellIndex.B00, BellIndex.B00, BellIndex.B00],
                "generation_duration": [10, 10, 10, 10]}
        dataframe = RepchainDataFrameHolder(data=data).dataframe
        success_rate, success_rate_error, success_prob, success_prob_error, rate, rate_error = \
            bqc_test_two_qubits_success_rate(dataframe=dataframe, coherence_time=0.)
        assert success_prob_error > 0.
        assert rate_error == 0.
        assert success_rate_error > 0.
        assert 0 < success_prob < 1
        assert np.isclose(rate, 1 / 20)
        assert np.isclose(success_rate, success_prob * rate)


def test_two_qubit_bqc_test_success_probability():
    # perfect states and memory
    assert np.isclose(_two_qubit_bqc_test_success_probability(state_1=b00,
                                                              state_2=b00,
                                                              time_between_states=1.E8,
                                                              coherence_time=0.), 1.)
    # noisy states
    # both undergo an X error, which puts the dummy fidelity to 0 and the trap fidelity to 0.5
    assert np.isclose(_two_qubit_bqc_test_success_probability(state_1=b01,
                                                              state_2=b01,
                                                              time_between_states=1.E8,
                                                              coherence_time=0.), .5)
    # bad memory: success probability should become .5 because all qubits are maximally mixed
    assert np.isclose(_two_qubit_bqc_test_success_probability(state_1=b00,
                                                              state_2=b00,
                                                              time_between_states=1.E10,
                                                              coherence_time=1.), .5)


def test_average_fidelity_poles():
    # perfect case
    assert np.isclose(_average_fidelity_poles(b00), 1.)
    # bit-flipped case
    assert np.isclose(_average_fidelity_poles(b01), 0.)
    # Z error case
    assert np.isclose(_average_fidelity_poles(b10), 1.)


def test_average_fidelity_equator():
    # perfect case
    assert np.isclose(_average_fidelity_equator(b00), 1.)
    # Z error case
    assert np.isclose(_average_fidelity_equator(b10), 0.)
    # bit-flipped case
    assert np.isclose(_average_fidelity_equator(b01), 0.5)  # half the states undergo noise
