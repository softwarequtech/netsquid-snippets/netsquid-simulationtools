from netsquid_simulationtools.linear_algebra import _convert_to_density_matrix, xyz_eigenstates, XYZEigenstateIndex, \
    _fidelity_between_single_qubit_states, _perform_pauli_correction
from netsquid_simulationtools.process_teleportation import determine_teleportation_output_state
from netsquid_simulationtools.repchain_data_functions import _expected_target_state
import numpy as np
from math import floor


def bqc_test_two_qubits_success_rate(dataframe, coherence_time):
    """Rate at which test-round successes are obtained for two-qubit Verified Blind Quantum Computation (VBQC).

    This function uses entanglement-generation data to calculate the rate at which successful test rounds of a two-qubit
    Verified Blind Quantum Computation (VBQC) could be performed.
    The VBQC protocol considered here is the one described in
    Verifying BQP Computations on Noisy Devices with Minimal Overhead, Leichtle et al., 2021,
    https://arxiv.org/abs/2109.04042 .

    The hypothetical protocol considered here is the repeated execution of VBQC test rounds.
    Henceforth, the protocol will be referred to as BQC test protocol and is defined as follows:
    1. A client sends a qubit to a server (using quantum teleportation).
    2. The client sends another qubit to the server.
       The first qubit is stored in quantum memory at the server in the meantime.
    3. The server performs an entangling gate between the qubits and measures them in angles specified by the client.
    4. The server sends the measurement outcomes to the client.
    5. Start again.

    The client prepares one of the two qubits in a trap state (|0> or |1>) such that it is easy to predict what output
    the server should return if it is honest and there is no noise.
    One round of the protocol is considered a success if the server sends the expected output
    and a failure if the output deviates.
    Due to noise, there will be failures even if the server is honest.
    We assume the server is always honest in the test protocol.

    The success rate is then the number of rounds of the protocol can be executed with a success result per time unit,
    i.e., it is the rate at which rounds can be executed multiplied by the success probability of the rounds.
    While the BQC test protocol is in itself not an interesting application of a quantum network, it can be considered
    a benchmark for how well the network is suited to VBQC and possibly other multi-qubit applications.
    Compared to a benchmark protocol such as QKD, it has the compelling feature that it actually requires the
    distribution of multiple entangled states and the storage of qubits between rounds, making it a more meaningful
    benchmark for quantum-network applications that require multiple live qubits simultaneously.

    It is assumed that the quantum memory at the server is of the depolarizing type, i.e., that qubits undergo
    rho -> p rho + (1 - p) / 2 * Id with p = e^(- t / coherence_time), where t is the storage duration.
    It is also assumed that the client and server have perfect local operations.

    Notes
    -----
    In order to estimate the success rate, this function cuts the obtained data into pairs of subsequently distributed
    entangled states. Each such pair can then be mapped to a hypothetical execution of one round of the BQC test
    protocol as follows:
    - The client sends its two qubits to the server through quantum teleportation, where the entangled states in the
     data are the resource states and there is no noise except for any noise that may be in the states in the data.
    - After the start of the protocol round, it takes time t1 before the first qubit is teleported and t1 + t2 before
     the second qubit is teleported, where t1 is the time it took to distribute the first state and t2 the time it took
     to distribute the second state.
    - The first qubit that is teleported to the server is stored in depolarizing memory for a time t2.
    - It is chosen uniformly at random which of the two qubits is the trap and which is the dummy.

    For each of these hypothetical rounds the success probability and duration is calculated, resulting in a set of
    samples of both the success probabilities and durations. These quantities can be used to estimate the success rate;
    we estimate the success rate as the sample mean of the success probabilities divided by the sample mean.

    We quantify the error in this estimate using the standard deviation of the mean.
    For a ratio A / B, the variance of the mean (i.e., the standard deviation of the mean squared) can be expressed as
    Var_of_mean(A / B)= A / B * sqrt(Var_of_mean(A) / A ^ 2 + Var_of_mean(B) / B ^ 2 - 2 * Cov_of_means(A, B) / A / B.
    The variances and covariance of the means of A and B are equal to the variances and covariance of A and B themselves
    divided by the size of the sample.
    These variances and covariances can themselves be estimated using the sample variances and sample covariance.
    (Here, we use the unbiased definition for the sample (co)variance; i.e., with normalization factor 1 / (N - 1)
    instead of 1 / N, where N is the sample size. For more information, see
    [wikipedia](https://en.wikipedia.org/wiki/Sample_mean_and_covariance#Unbiasedness).)

    To calculate the success probability of one of the hypothetical rounds of the BQC test protocol, we use
    Equation (B22) in the following paper:
    Requirements for a processing-node quantum repeater on a real-world fiber grid, Avis et al, 2022,
    http://arxiv.org/abs/2207.10579 .
    The success probability is obtained as p_succ = 1 - p_fail, where p_fail is given by the Equation (B22).
    In this equation, the quantity F_dummy is the average fidelity of its corresponding teleportation channel
    over the poles of the Bloch sphere, as defined in Equation (B17).
    The quantity F_trap is the average fidelity of its corresponding teleportation channel over a set of eight states on
    the equator of the Bloch sphere as defined in Equations (B18) and (B8).

    F_trap in this function is determined by taking an average of the teleportation channel over the entire equator of
    the Bloch sphere instead of over the eight states as defined in the paper.
    The reason for this is that, together, the eight states describe two intersecting squares on the equator of the
    Bloch sphere.
    As the average of a channel over any square on the equator of the Bloch sphere is equal to the average over the
    entire equator, the average over two such squares is also equal to the average over the equator.
    Therefore, F_trap is equal to the average over the equator.
    For more information about this and about how the average over the equator is calculated, see the documentation of
    :func:`~netsquid_simulatontools.process_bqc._average_fidelity_equator`.

    As it is determined uniformly at random which of the two states in the data is used to teleport the dummy and
    which to teleport the trap, the success probability is determined as the average over the two choices.

    Parameters
    ----------
    dataframe : pandas.DataFrame
        Dataframe containing entanglement-distribution data.
        For specification of how the data should be structured, see the documentation of
        :class:`~netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder`.
    coherence_time : float
        Coherence time [s] of the quantum memory used to store qubits at the VBQC server,
        i.e., the time after which the depolarizing parameter becomes 1 / e.
        Set to 0 to turn off depolarizing noise.

    Returns
    -------
    float
        Estimated success rate [1/s].
    float
        Standard error in estimated success rate [1/s].
    float
        Estimated success probability.
    float
        Standard error in estimated success probability.
    float
        Estimated test rate [1/s].
    float
        Standard error in estimated test rate [1/s].

    """
    success_probs, durations = _bqc_success_probabilities_and_durations(dataframe=dataframe,
                                                                        coherence_time=coherence_time)

    # mean success rate
    mean_success_prob = np.mean(success_probs)
    mean_duration = np.mean(durations)
    success_rate = mean_success_prob / mean_duration

    # error in success rate
    covariance_matrix = np.cov(success_probs, durations, bias=True)
    var_success_probs = covariance_matrix[0][0]
    var_durations = covariance_matrix[1][1]
    covariance = covariance_matrix[0][1]
    success_rate_error = success_rate * np.sqrt(var_success_probs / mean_success_prob ** 2 +
                                                var_durations / mean_duration ** 2 -
                                                2 * covariance / (mean_success_prob * mean_duration))
    success_rate_error /= np.sqrt(len(success_probs))

    # the success probability
    success_prob = mean_success_prob
    success_prob_error = np.sqrt(var_success_probs) / np.sqrt(len(success_probs))

    # the test rate
    test_rate = 1 / mean_duration
    test_rate_error = test_rate ** 2 * np.sqrt(var_durations) / np.sqrt(len(durations))

    return success_rate, success_rate_error, success_prob, success_prob_error, test_rate, test_rate_error


def _bqc_success_probabilities_and_durations(dataframe, coherence_time):
    """Use data for entanglement generation to create samples of success probabilities and durations of BQC test rounds.

    For more information, see the documentation of
    :func:`netsquid_simulationtools.process_bqc.bqc_test_two_qubits_success_rate`.

    Parameters
    ----------
    dataframe : pandas.DataFrame
        Dataframe containing entanglement-distribution data.
        For specification of how the data should be structured, see the documentation of
        :class:`~netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder`.
    coherence_time : float
        Coherence time [s] of the quantum memory used to store qubits at the VBQC server,
        i.e., the time after which the depolarizing parameter becomes 1 / e.
        Set to 0 to turn off depolarizing noise.

    """
    states = []
    for index, row in dataframe.iterrows():
        state = _convert_to_density_matrix(row["state"])
        state = _perform_pauli_correction(state=state, bell_index=_expected_target_state(row))
        states.append(state)

    state_distribution_times = list(dataframe["generation_duration"])

    assert len(states) == len(state_distribution_times)
    number_of_bqc_tests = floor(len(states) / 2)

    success_probabilities = [
        _two_qubit_bqc_test_success_probability(state_1=states[2 * i], state_2=states[2 * i + 1],
                                                time_between_states=state_distribution_times[2 * i + 1],
                                                coherence_time=coherence_time)
        for i in range(number_of_bqc_tests)
    ]

    durations = [
        state_distribution_times[2 * i] + state_distribution_times[2 * i + 1]
        for i in range(number_of_bqc_tests)
    ]

    return success_probabilities, durations


def _two_qubit_bqc_test_success_probability(state_1, state_2, time_between_states, coherence_time):
    """Calculate the success probability of a single round of the BQC test protocol.

    For more information, see the documentation of
    :func:`netsquid_simulationtools.process_bqc.bqc_test_two_qubits_success_rate`.

    Parameters
    ----------
    state_1 : numpy.array
        Two-qubit entangled state used to teleport the first qubit in the two-qubit BQC test round.
    state_2 : numpy.array
        Two-qubit entangled state used to teleport the second qubit in the two-qubit BQC test round.
    time_between_states : float
        Time (arbitrary units) between the distribution of the two entangled states.
        Important that the same time units are used as for `coherence_time`.
    coherence_time : float
        Coherence time (arbitrary units) of the quantum memory used to store qubits at the VBQC server,
        i.e., the time after which the depolarizing parameter becomes 1 / e.
        Set to 0 to turn off depolarizing noise.
        Important that the same time units are used as for `time_between_states`.

    """

    # depolarizing parameter due to storage in the server's quantum memory
    depolar_param = np.exp(- time_between_states / coherence_time) if coherence_time > 0. else 1.

    # calculate separately the success probability for the two choices that can be made for which state is used as
    # dummy and which as trap
    fidelities_pole = [_average_fidelity_poles(state_1), _average_fidelity_poles(state_2)]
    fidelities_equator = [_average_fidelity_equator(state_2), _average_fidelity_equator(state_1)]
    success_probs = []
    for fid_pole, fid_equator in zip(fidelities_pole, fidelities_equator):
        # This calculates the success probability if it is fixed which entangled state is used to teleport the dummy
        # qubit and which the trap, and it is chosen uniformly at random which state on the poles of the Bloch sphere
        # the dummy is prepared in and which state on the equator of the Bloch sphere the trap is prepared in.
        # Based on Equation (B22) in arXiv:2207.10579 .
        failure_probability = depolar_param * (fid_pole + fid_equator - 2 * fid_pole * fid_equator)
        failure_probability += (1 - depolar_param) / 2
        success_probs.append(1 - failure_probability)

    return np.mean(success_probs)


def _average_fidelity_poles(state):
    """Average fidelity of single-qubit state over the poles of the Bloch sphere.

    For input density matrix rho, this function returns (<0|rho|0> + <1|rho|1>) / 2.
    If the input state is a ket vector |psi>, the density matrix rho is determined using rho = |psi><psi|.

    Parameters
    ----------
    state : numpy.array
        Single-qubit state for which to calculate the average fidelity over the Bloch-sphere poles.

    Returns
    -------
    float
        Average fidelity of the input state over the poles of the Bloch sphere, i.e., over |0> and |1>.

    """
    z0 = xyz_eigenstates[XYZEigenstateIndex.Z0]
    z1 = xyz_eigenstates[XYZEigenstateIndex.Z1]

    fidelities = []
    for information_state in [z0, z1]:
        output_state = determine_teleportation_output_state(information_state=information_state,
                                                            resource_state=state)
        fidelity = _fidelity_between_single_qubit_states(output_state, information_state)
        fidelities.append(fidelity)

    return np.mean(fidelities)


def _average_fidelity_equator(state):
    """Average fidelity of single-qubit state over the equator of the Bloch sphere.

    For input density matrix rho, this function returns (<+|rho|+> + <-|rho|-> + <Y+|rho|Y+> + <Y-|rho|Y->) / 4.
    Here, |Y+> and |Y-> are the Pauli Y eigenstates with eigenvalues +1 and -1 respectively, i.e.,
    |Y+> = (|0> + i|1>) / sqrt(2), |Y-> = (|0> - i|1>) / sqrt(2).
    If the input state is a ket vector |psi>, the density matrix rho is determined using rho = |psi><psi|.

    It can be shown that the average over four points on the equator of a Bloch sphere forming a square is equal
    to the average over the full equator using the Haar measure.
    Therefore, the output of this function is equal to
    1 / (2 pi) Integral_{equator} <psi|rho|psi> d psi.
    This can be shown by replicating the proof in
    Fidelity of single qubit maps, Bowdrey et al., 2002, https://arxiv.org/abs/quant-ph/0201106 ,
    but with the Z index dropped.

    Parameters
    ----------
    state : numpy.array
        Single-qubit state for which to calculate the average fidelity over the Bloch-sphere poles.

    Returns
    -------
    float
        Average fidelity of the input state over the equator of the Bloch sphere.

    """
    x0 = xyz_eigenstates[XYZEigenstateIndex.X0]
    x1 = xyz_eigenstates[XYZEigenstateIndex.X1]
    y0 = xyz_eigenstates[XYZEigenstateIndex.Y0]
    y1 = xyz_eigenstates[XYZEigenstateIndex.Y1]

    fidelities = []
    for information_state in [x0, x1, y0, y1]:
        output_state = determine_teleportation_output_state(information_state=information_state,
                                                            resource_state=state)
        fidelity = _fidelity_between_single_qubit_states(output_state, information_state)
        fidelities.append(fidelity)

    return np.mean(fidelities)
