import numpy as np
from netsquid import BellIndex

from netsquid_simulationtools.linear_algebra import _convert_to_density_matrix
from netsquid_simulationtools.repchain_data_functions import estimate_duration_per_success, _expected_target_state


HADAMARD = np.array([[1, 1], [1, -1]]) / np.sqrt(2)
Y_ROTATOR = np.array([[1, 1], [1j, -1j]]) / np.sqrt(2)
BASIS_CHANGE_OPERATORS = {"Z": np.eye(2),
                          "X": HADAMARD,
                          "Y": Y_ROTATOR}


def estimate_bb84_secret_key_rate_from_data(dataframe, use_state_data_as_default=True, sifting_factor=1):
    """Estimate the secret-key rate of BB84 in the asymptotic limit based on entanglement-distribution data.

    The data should contain results of a (simulated) entanglement-distribution experiment, where two parties (Alice
    and Bob) share a Bell state and perform a measurement in either the X, Y or Z basis on their qubit.
    The data should detail how long it took to share each Bell state.
    Additionally, it should contain at least one of the following:
    1. What the basis was that each party measured each Bell state in, and what the outcome of the measurement was.
    2. What the (mixed) state was that was distributed between Alice and Bob.
    We refer to the first as "measurement data" and to the second as "state data".
    The data can be used to estimate the rate of entanglement distribution and the Quantum-Bit Error Rate (QBER),
    from which the secret-key rate is calculated using

    max(0., 1 - H(qber_x) - H(qber_z)) * sifting_factor / avg_number_attempts_per_success = SKR in [bits/attempt],

    where H(p) is the binary entropy function

    H(p) = -p log(p) - (1-p) log(1-p),

    and qber_x and qber_z are the QBER when both Alice and Bob measure in the X or Z basis respectively.

    If measurement data is provided, the QBER is calculated by determining how often the measurements of Alice and Bob
    are (anti)correlated as would be expected based on the entangled state Alice and Bob expect to share.
    If state data is provided, the QBER is calculated by determining how often the correct correlation would be
    obtained if Alice and Bob would perform perfect measurements on their qubits.
    If both measurement and state data are provided, the state data is used by default unless
    `use_state_data_as_default` is set to `False`.
    The state data can sometimes provide more statistical information than the measurement outcomes,
    but if it is e.g. important to model noise in the measurement process it can be desirable to use the measurement
    data instead.
    For more information on QBER calculation, see :func:`netsquid_simulationtools.process_qkd.qber`.

    Note: measurement data can also represent the more traditional scenario where BB84 is performed without
    entanglement, i.e. Alice sends states that Bob measures.
    In that case, Alice's basis choice represents the basis in which she prepares her state, and her measurement
    outcome represents which of the two corresponding basis states she sends.

    Parameters
    ----------
    dataframe : pandas.DataFrame
        Dataframe containing entanglement-distribution data.
        For specification of how the data should be structured, see the documentation of
        :class:`~netsquid_simulationtools.repchain_dataframe_holder.RepchainDataFrameHolder`.
    use_state_data_as_default : bool, optional
        Whether state data or measurement data should be used in case both are available.
        Set true for state data (default) and false for measurement data.
    sifting_factor : float (optional)
        Probability that both Alice and Bob use the same measurement basis.
        Defaults to 1, representing fully-asymmetric BB84 (see notes below for explanation).
        If both bases are chosen individually at random, `sifting_factor` should be set to 0.5.

    Returns
    -------
    secret_key_rate : float
        Secret Key Rate in [bits/attempt]. Note: The User can than convert this into [bits/second] or [bits/channel_use]
    skr_min : float
        Minimal value of Secret Key Rate within the interval given by the Stdev of the Qber
    skr_max : float
        Maximal value of Secret Key Rate within the interval given by the Stdev of the Qber
    skr_error : float
        Symmetric error calculated from the standard deviations of the QBERs and the number of attempts/success.

    Notes
    -----

    __Protocol__

    The protocol considered here is the one described by
    Key rates for quantum key distribution protocols with asymmetric noise,
    Murta et al, 2020, https://journals.aps.org/pra/abstract/10.1103/PhysRevA.101.062321 ,
    page 2 (entanglement-based version without advantage distillation).


    __Sifting__

    In the paper
    Efficient Quantum Key Distribution Scheme and a Proof of its Unconditional Security,
    Lo et al, 2005, https://doi.org/10.1007/s00145-004-0142-y .
    they show that a sifting factor of 1 can be obtained by biasing basis choices to either the Z or X basis.
    The preferred basis is then used to generate key, while the other one is only used for parameter estimation
    (i.e. checking that the QBER is not too large, as required to ensure security).
    The bias can be made sufficiently large such that, in the asymptotic limit, the number of results for which
    Alice and Bob did not both use the preferred basis becomes negligible, resulting in a sifting factor of 1.
    Lo et al. show that the security of this protocol is unconditional.

    Which basis is used as preferred basis (key-generation basis) does not affect the secret-key rate,
    see Proposition 1 of

    Because of this, a default sifting factor of 1 is used in this function.
    However, it is left as a parameter in case the user does want to include sifting.
    E.g. if symmetric BB84 is considered (i.e. choosing measurement bases uniformly at random), a sifting factor
    of 1/2 should be chosen.


    __Parameter Estimation__

    The effects of parameter estimation (which has to be performed for both bases) are negligible
    in the asymptotic limit.
    In Lo et al, it is mentioned that the number of sacrificed results must be at least of order log(k),
    where k is the length of the final key.
    In the limit where k goes to infinity, log(k) / k goes to zero, and thus the fraction of sacrificed results becomes
    negligible.

    In the paper
    A largely self-contained and complete security proof for quantum key distribution,
    Tomamichel and Leverrier, 2017, https://doi.org/10.22331/q-2017-07-14-14 ,
    the number of sacrificed results that is used is sqrt(m), where m is the total number of (sifted) results
    (k = sqrt(m), between equations (58) and (59); note that here k means the number of results sacrificed
    for parameter estimation, which is different from the meaning in Lo et al.).
    Since sqrt(m) / m also goes to zero in the asymptotic limit, it seems safe to assume the number of sacrificed
    results is negligible.

    Therefore, we do not include any effects of parameter estimation in this function.

    Note that in the originally proposed BB84 protocol, half the results are lost due to sifting.
    Furthermore, half of the remaining results are then used for parameter estimation.
    Therefore, the secret-key rate calculated in this function (when using the default sifting factor of 1)
    differs by a factor of 4 from the secret-key rate that would have been achieved
    using the original protocol in the asymptotic limit.


    __Secret-Key Rate__

    For a derivation of the secret-key rate formula used in this function, see e.g.
    The security of practical quantum key distribution,
    Scarani et al, 2009, https://arxiv.org/ct?url=https%3A%2F%2Fdx.doi.org%2F10.1103%2FRevModPhys.81.1301&v=4740d283 ,
    appendix A.
    See also e.g. Murta et al, eq. (4), or Tomamichel and Leverrier, section 5, or
    Simple Proof of Security of the BB84 Quantum Key Distribution Protocol
    Shor and Preskill, 2000, https://link.aps.org/doi/10.1103/PhysRevLett.85.441
    (left column, above the lower equation).

    These sources don't always define the secret-key rate the same way. Specifically, some include sifting and
    parameter estimation, and some don't (and sometimes it's just not clear).
    However, in the case where both the effects of sifting and parameter estimation on the secret-key rate vanish,
    as we are considering, the differences don't really matter.


    __Sifting in Data__

    For estimating the QBER from measurement data, all results where the measurement bases are unequal are disregarded.
    However, all results are taken into account when calculating the number of attempts per success.
    To estimate the number of attempts per success, results for which the measurement bases are unequal are also
    considered a "success".
    Any sifting is only applied in post-processing, as determined by the `sifting_factor` parameter in this function.
    Thus, the sifting factor is never extracted from the data.
    If basis choices are not always the same in the data, this does not have an effect on the calculated
    secret-key rate.
    (except that the uncertainty in the QBER will be larger compared to the case where the basis choices
    in the data would always be aligned, since then there would have been more results usable for estimating the QBER).
    For example, in case measurement bases are chosen randomly in the data such that the same basis is used only
    in 1/2 of the cases, but the default value `sifting_factor = 1` is used,
    this does not mean that the secret-key rate outputted by this function is halved as compared to that for the same
    data set but with all results with unequal basis choice removed.

    """
    if dataframe.empty:
        return 0, 0, 0, 0

    qber_x, qber_x_error = qber(dataframe=dataframe, basis_a="X", basis_b=None,
                                use_state_data_as_default=use_state_data_as_default)
    qber_z, qber_z_error = qber(dataframe=dataframe, basis_a="Z", basis_b=None,
                                use_state_data_as_default=use_state_data_as_default)

    duration_per_success, duration_per_success_error = estimate_duration_per_success(dataframe)

    secret_key_rate, skr_min, skr_max, skr_error = _estimate_bb84_secret_key_rate(qber_x, qber_x_error,
                                                                                  qber_z, qber_z_error,
                                                                                  duration_per_success,
                                                                                  duration_per_success_error)

    return (secret_key_rate * sifting_factor, skr_min * sifting_factor,
            skr_max * sifting_factor, skr_error * sifting_factor)


def qber(dataframe, basis_a, basis_b=None, use_state_data_as_default=True, quantile=1):
    """Function that estimates the QBER for specified measurement bases using a data set.

    The function uses either state data or measurement data present in `dataframe` to estimate the qber.
    That means that every data point in the data (i.e., row of `dataframe`) must either contain an entry
    'state' with a valid quantum state as entry, or the entries 'basis_A', 'basis_B', 'outcome_A' and 'outcome_B',
    with the measurement bases and outcomes as values respectively.
    For what qualifies as a 'valid quantum state', see the documentation of
    :func:`netsquid_simulationtools.linear_algebra._convert_to_density_matrix`.

    If neither state data nor measurement data is available, a `ValueError` is raised.
    If both are available, by default the state data is used instead of the measurement data as it contains
    more statistical information than the outcomes.
    To instead use measurement data when both are available, set `use_state_data_as_default` to false.
    This should e.g., be done when simulations also model errors in the measurement process itself;
    when the qber is extracted from state data, it is determined how likely an error would be if the state
    was measured perfectly.

    Parameters
    ----------
    dataframe : pandas.DataFrame
        DataFrame containing simulation data. Can contain either state data or measurement data.
    basis_a : string
        Basis in which Alice measured. Should be in ["X", "Y", "Z"].
    basis_b : string , optional
        Basis in which Bob measured, if not specified will be set to the same basis as Alice by default.
    use_state_data_as_default : bool, optional
        Whether state data or measurement data should be used in case both are available.
        Set true for state data (default) and false for measurement data.
    quantile : float , optional
        1 - alpha/2 quantile of a standard normal distribution corresponding to the target error rate alpha.
        For a 95% confidence level, the error alpha = 1 − 0.95 = 0.05 , so (1 − alpha/2) = 0.975 and quantile = 1.96.
        Default is quantile = 1

    Returns
    -------
    qber : float
        QBER (quantum bit error rate)
    qber_error : float
        QBER error (standard deviation of the mean)

    Raises
    ------
    ValueError
        If `dataframe` does not contain any data to estimate the qber with for the requested bases.

    """
    if basis_b is None:
        basis_b = basis_a

    qbers = []
    for index, row in dataframe.iterrows():

        contains_state_data = "state" in row
        if contains_state_data:
            try:
                state = _convert_to_density_matrix(row["state"])
            except TypeError:
                # if the state cannot be interpreted as a state, there is effectively no state data
                # (probably the state is None or some other placeholder)
                contains_state_data = False

        contains_measurement_data = "basis_A" in row and "basis_B" in row and "outcome_A" in row and "outcome_B" in row

        if not (contains_state_data or contains_measurement_data):
            raise ValueError("Data contains neither state data nor measurement data, so QBER cannot be calculated.")

        if contains_state_data and contains_measurement_data:
            # if both types of data are present, state data is used by default if use_state_date_as_default is true
            # if it is false, we set contains_state_data to false so that below the measurement data is used instead
            contains_state_data = use_state_data_as_default

        expect_correlation = _do_we_expect_correlation(row=row, basis_a=basis_a, basis_b=basis_b)

        if contains_state_data:
            qbers.append(_qber_from_state(state=state, basis_a=basis_a, basis_b=basis_b,
                                          expect_correlation=expect_correlation))

        else:
            if row["basis_A"] == basis_a and row["basis_B"] == basis_b:  # if measurement data matches specified bases
                correlation = row["outcome_A"] == row["outcome_B"]
                qbers.append(float(correlation != expect_correlation))

    if len(qbers) == 0:
        raise ValueError(f"No data available to estimate the QBER for basis_A: {basis_a}, basis_B: {basis_b}.")

    qber = np.mean(qbers)
    qber_error = quantile * np.std(qbers) / np.sqrt(len(qbers))
    return qber, qber_error


def _qber_from_state(state, basis_a, basis_b, expect_correlation):
    """Calculate quantum-bit error rate that would be obtained from two-qubit state.

    This function assumes measurements are performed perfectly.
    It returns the probability that if two parties share `state` and measure in `basis_a` and `basis_b` respectively,
    that the outcomes are (anti)correlated (as determined by the parameter `expect_correlation`).

    Parameters
    ----------
    state
        Something that can be interpreted as two-qubit quantum state.
        E.g., length-4 vector or 4x4 density matrix.
        For more information on data formats that can be interpreted as quantum state, see the documentation of
        :func:`netsquid_simulationtools.linear_algebra._convert_to_density_matrix`.
    basis_a : string
        Basis in which Alice measures. Should be in ["X", "Y", "Z"].
    basis_b : string , optional
        Basis in which Bob measures, if not specified will be set to the same basis as Alice by default.
    expect_correlation : bool
        Whether correlation (True) or anticorrelation (False) between the measurement results is expected.
        Changing the value of this parameter will flip the qber to 1 - qber as the success probabilty becomes the
        errror probability.

    Returns
    -------
    float
        qber

    """
    state = _convert_to_density_matrix(state)
    povm_same_outcome = np.diag([1, 0, 0, 1])
    rotation_operator_a = BASIS_CHANGE_OPERATORS[basis_a]
    rotation_operator_b = BASIS_CHANGE_OPERATORS[basis_b]
    rotation_operator = np.kron(rotation_operator_a, rotation_operator_b)
    povm_same_outcome = rotation_operator @ povm_same_outcome @ ((rotation_operator.T).conj())
    prob_correlation = np.real(np.trace(povm_same_outcome @ state))
    qber = 1 - prob_correlation if expect_correlation else prob_correlation
    return qber


def _binary_entropy(p):
    """Calculate binary entropy.

    H(p) = -p log(p) - (1-p) log(1-p)

    Parameters
    ----------
    p : float
        Probability value to calculate binary entropy for.


    Returns
    -------
    float
        Binary entropy.

    """
    a = - p * np.log2(p) if p > 0 else 0
    b = - (1 - p) * np.log2(1 - p) if p < 1 else 0
    return a + b


def _derivative_binary_entropy(x):
    """Derivative of binary-entropy function.

    Parameters
    ----------
    x : float in the open interval (0, 1)
        Value at which to evaluate the function.

    Returns
    -------
    float

    """
    return - np.log2(x / (1 - x))


def _error_binary_entropy(x, x_error):
    """Standard error in the binary-entropy function.

    Parameters
    ----------
    x : float in the closed interval [0, 1]
        Estimated value of the argument.
    x_error : float
        Standard error of the argument.

    Returns
    -------
    float

    Note
    ----
    Only accurate when x_error << 1.

    """
    if np.isclose(x, 0) or np.isclose(x, 1) or x_error == 0:
        return 0
    return _derivative_binary_entropy(x) * x_error


def _error_secret_key_rate(qber_x, qber_x_error, qber_z, qber_z_error,
                           attempts_per_success, attempts_per_success_error):
    """Calculating the standard error in the secret-key rate.

    Parameters
    ----------
    qber_x : float (between 0 and 1)
        Quantum Bit Error Rate in X basis (both Alice and Bob measure in X)
    qber_x_error : float
        Standard deviation of the Quantum Bit Error Rate in X basis
    qber_z : float (between 0 and 1)
        Quantum Bit Error Rate in Z basis (both Alice and Bob measure in Z)
    qber_z_error : float
        Standard deviation of the Quantum Bit Error Rate in Z basis
    attempts_per_success: float
        Average number of attempts required per successfully-distributed raw key bit.
    attempts_per_success_error: float
        Standard deviation of the number of attempts required per successfully-distributed raw key bit.

    Returns
    -------
    float

    """
    secret_fraction = 1 - _binary_entropy(qber_x) - _binary_entropy(qber_z)
    if secret_fraction <= 0:
        return 0
    error_secret_fraction_squared = (_error_binary_entropy(qber_x, qber_x_error) ** 2 +
                                     _error_binary_entropy(qber_z, qber_z_error) ** 2)

    error_rate_squared = (attempts_per_success_error / attempts_per_success ** 2) ** 2

    error_secret_key_rate_squared = (error_secret_fraction_squared / attempts_per_success ** 2 +
                                     error_rate_squared * secret_fraction ** 2)

    return np.sqrt(error_secret_key_rate_squared)


def _estimate_bb84_secret_key_rate(qber_x, qber_x_error, qber_z, qber_z_error,
                                   attempts_per_success, attempts_per_success_error):
    """ Function that computes the secret key rate and its standard deviation from the simulation data in the input
    Dataframe.

    This is done by calculating the Quantum Bit Error Rate (QBER) in X and Z basis and then taking:
    max(0., 1 - H(qber_x) - H(qber_z)) / attempts_per_success

    where H(p) is the binary entropy function:
    H(p) = -p log(p) - (1-p) log(1-p)

    For more information about derivation of the formula, the protocol under consideration and the assumptions used,
    see the docstring of
    :func:`~netsquid_simulationtools.repchain_data_functions.estimate_bb84_secret_key_rate_from_data`.

    Parameters
    ----------
    qber_x : float (between 0 and 1)
        Quantum Bit Error Rate in X basis (both Alice and Bob measure in X)
    qber_x_error : float
        Standard deviation of the Quantum Bit Error Rate in X basis
    qber_z : float (between 0 and 1)
        Quantum Bit Error Rate in Z basis (both Alice and Bob measure in Z)
    qber_z_error : float
        Standard deviation of the Quantum Bit Error Rate in Z basis
    attempts_per_success: float
        Average number of attempts required per successfully-distributed raw key bit.
    attempts_per_success_error: float
        Standard deviation of the number of attempts required per successfully-distributed raw key bit.


    Returns
    -------
    secret_key_rate : float
        Secret Key Rate in [bits/attempt] as max(0., 1 - H(qber_x) - H(qber_z)) / attempts_per_success.
    skr_min : float
        Minimal value of Secret Key Rate within the interval given by the Stdev of the Qber.
    skr_max : float
        Maximal value of Secret Key Rate within the interval given by the Stdev of the Qber.
    skr_error : float
        Standard error in the secret-key rate (calculated using standard formula for propagation of uncertainty).

    """
    # calculate secret key rate using binary entropy function
    secret_key_rate = max(0., 1 - _binary_entropy(qber_x) - _binary_entropy(qber_z)) / attempts_per_success

    if (qber_x, qber_z) <= (0, 1):
        print("Beware: one of the QBER's is 0 or 1. "
              "This may indicate not enough statistics were obtained to estimate the error.")

    # pick min / max in interval [0,1]
    skr_min = min(1 - _binary_entropy(qber_x + qber_x_error) - _binary_entropy(qber_z + qber_z_error),
                  1 - _binary_entropy(qber_x + qber_x_error) - _binary_entropy(qber_z - qber_z_error),
                  1 - _binary_entropy(qber_x - qber_x_error) - _binary_entropy(qber_z + qber_z_error),
                  1 - _binary_entropy(qber_x - qber_x_error) - _binary_entropy(qber_z - qber_z_error),
                  1 - _binary_entropy(qber_x) - _binary_entropy(qber_z),
                  1.)
    skr_min = max(0., skr_min) / (attempts_per_success + attempts_per_success_error)
    skr_max = max(1 - _binary_entropy(qber_x + qber_x_error) - _binary_entropy(qber_z + qber_z_error),
                  1 - _binary_entropy(qber_x + qber_x_error) - _binary_entropy(qber_z - qber_z_error),
                  1 - _binary_entropy(qber_x - qber_x_error) - _binary_entropy(qber_z + qber_z_error),
                  1 - _binary_entropy(qber_x - qber_x_error) - _binary_entropy(qber_z - qber_z_error),
                  1 - _binary_entropy(qber_x) - _binary_entropy(qber_z),
                  0.)
    skr_max = min(1., skr_max) / (attempts_per_success - attempts_per_success_error)

    skr_error = _error_secret_key_rate(qber_x, qber_x_error, qber_z, qber_z_error,
                                       attempts_per_success, attempts_per_success_error)

    return secret_key_rate, skr_min, skr_max, skr_error


def _do_we_expect_correlation(row, basis_a, basis_b):
    """Function that computes the expected correlation of measurement results between Alice and Bob for a row of the
    input DataFrame.

    This is done by calculating the expected target state and then looking up its expected correlation for the basis
    choice specified in this row of the DataFrame.

    Parameters
    ----------
    row : pandas.Series
        Row of the DataFrame containing simulation data

    Returns
    -------
    bool
        Boolean, whether we expect measurement results between Alice and Bob to be correlated.

    Note: For indexing of Bell stated see `_expected_target_state()`

    """
    if basis_a != basis_b:
        # TODO: maybe this should not throw an error but just ignore the line?
        raise ValueError("Cannot get correlation for Bell states if A and B measured in different basis.")

    # when using atomic ensembles with presence-absence encoding, two chains have to be used. The expected correlation
    # depends on the target state of both chains
    if 'chain_2_midpoint_0' in row.keys():
        if basis_a == "X":
            expected_index_chain_1, expected_index_chain_2 = _expected_target_state(row)
            if expected_index_chain_1 == expected_index_chain_2:
                return True
            else:
                return False
        elif basis_a == "Z":
            return False
        elif basis_a == "Y":
            raise NotImplementedError("Y-basis correlations not implemented for two chain AE setup yet.")

    expected_target_index = _expected_target_state(row)
    basis_to_is_correlated = {"X": {BellIndex.PHI_PLUS: True,
                                    BellIndex.PSI_PLUS: True,
                                    BellIndex.PSI_MINUS: False,
                                    BellIndex.PHI_MINUS: False},
                              "Y": {BellIndex.PHI_PLUS: False,
                                    BellIndex.PSI_PLUS: True,
                                    BellIndex.PSI_MINUS: False,
                                    BellIndex.PHI_MINUS: True},
                              "Z": {BellIndex.PHI_PLUS: True,
                                    BellIndex.PSI_PLUS: False,
                                    BellIndex.PSI_MINUS: False,
                                    BellIndex.PHI_MINUS: True},
                              }
    return basis_to_is_correlated[basis_a][expected_target_index]
